# Concurrent and distributed programming #

This repo collects all the assignments delivered during the course of 'Concurrent and distributed programming' - University of Bologna.

[More info](http://www.engineeringarchitecture.unibo.it/en/programmes/course-unit-catalogue/course-unit/2016/412598)