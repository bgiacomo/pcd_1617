package asg3.ex1;

public class ViewWrapper {
	   
	private static View myView = null;
	   
	   private ViewWrapper() {
	      // Exists only to defeat instantiation.
		   myView=new View();
	   }
	   
	   public static View getInstance() {
	      if(myView == null) {
	    	  new ViewWrapper();
	    	  return myView;
	      }
	      return myView;
	   }
	   
	   
	}
