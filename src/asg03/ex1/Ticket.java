package asg3.ex1;

public class Ticket {

	private int turn;

	public Ticket(int turn) {
		super();
		this.turn = turn;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}
		
	
}
