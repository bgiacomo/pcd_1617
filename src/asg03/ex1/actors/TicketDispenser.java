package asg3.ex1.actors;

import java.util.ArrayList;
import java.util.LinkedList;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import asg3.ex1.Singleton;
import asg3.ex1.Ticket;
import asg3.ex1.Util;
import asg3.ex1.ViewWrapper;
import asg3.ex1.messages.BookingMsg;
import asg3.ex1.messages.KillingMsg;
import asg3.ex1.messages.LosingMsg;
import asg3.ex1.messages.StartGameMsg;
import asg3.ex1.messages.StartTurnMsg;
import asg3.ex1.messages.TryToGuessMsg;
import asg3.ex1.messages.WinningMsg;

public class TicketDispenser extends UntypedActor {

	private int nPlayer;
	private ArrayList<ActorRef> players;
	private LinkedList<ActorRef> queuePlayers;
	private int turn=0;
	private int round=0;
	private Util myUtil;
	
	public void preStart(){
		this.players = new ArrayList<>();	
		this.queuePlayers = new LinkedList<>();
		this.myUtil=new Util();
	}
	
	@Override
	public void onReceive(Object message) throws Throwable {
		// TODO Auto-generated method stub
		
		if(message instanceof StartGameMsg){
			System.out.println("TicketDispenser created.");
			nPlayer=((StartGameMsg) message).getNumPlayers();
			for (int i = 0; i < nPlayer; i++) {
				ActorRef player = getContext().actorOf(Props.create(PlayerActor.class, i,"Player"+i));
				players.add(player);	
			}
			players.stream()
				.forEach(p -> p.tell(new StartTurnMsg(), getSelf()));
			
		}else if(message instanceof BookingMsg){
			ActorRef player=getContext().sender();
			queuePlayers.add(player);
			if(queuePlayers.size()==myUtil.getNPlayers()){
				ActorRef unlockedPlayer=queuePlayers.get(0);
				unlockedPlayer.tell(new Ticket(turn), getSelf());
				queuePlayers.remove(0);
				turn++;
				if(turn==myUtil.getNPlayers()){
					turn = 0;
					round++;
					ViewWrapper.getInstance().setRoundToTurnLabel(round);
				}
			}
			
			
		}else if(message instanceof WinningMsg){
			int id = ((WinningMsg) message).getPlayerId();
			for (ActorRef p:players){
					p.tell(new LosingMsg(id), getSelf());
				}
			/* Thread.sleep is just to be sure that oracle and ticketDispenser stops after the players even
			if this is not explicitly required by the track */
			Thread.sleep(1000);
			ActorRef oracle=Singleton.getInstance();
			oracle.tell(new LosingMsg(id), getSelf());
			getContext().stop(getSelf());
			System.out.println("Ticket dispenser terminated.");
		}else if(message instanceof KillingMsg){
			
			for (int i = 0; i < nPlayer; i++) {
				players.get(i).tell(PoisonPill.getInstance(), ActorRef.noSender());
			}
			Singleton.getInstance().tell(PoisonPill.getInstance(), ActorRef.noSender());
			getContext().stop(getSelf());
			
		}
			
		}
		
}
	


