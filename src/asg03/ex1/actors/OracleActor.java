package asg3.ex1.actors;

import java.util.Random;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import asg3.ex1.Singleton;
import asg3.ex1.ViewWrapper;
import asg3.ex1.messages.KillingMsg;
import asg3.ex1.messages.LosingMsg;
import asg3.ex1.messages.ResponseMsg;
import asg3.ex1.messages.StartGameMsg;
import asg3.ex1.messages.TryToGuessMsg;
import asg3.ex1.messages.WinningMsg;

public class OracleActor extends UntypedActor {

	private int numberToBeGuessed;
	private ActorRef currentPlayer;
	private ActorRef ticketDispenser;
	
	@Override
	public void onReceive(Object message) throws Throwable {
		// TODO Auto-generated method stub
		
		if(message instanceof StartGameMsg){
			System.out.println("Oracle instantiated");
			numberToBeGuessed=generateRandomNumber();
			ViewWrapper.getInstance().setNumberToGuessLabel(numberToBeGuessed);
			createTicketDispenser();
		}else if(message instanceof TryToGuessMsg){
			currentPlayer = getContext().sender();
			int num = ((TryToGuessMsg) message).getNumber();
			int id = ((TryToGuessMsg) message).getId();
			System.out.println("Player "+id+": Is it "+num+"?");
			checkResponse(num,id);
		}else if(message instanceof LosingMsg){
			getContext().stop(getSelf());
			System.out.println("Oracle terminated.");
		}else if(message instanceof KillingMsg){
			ticketDispenser.tell(new KillingMsg(), getSender());
		}
		
	}
	
	private void checkResponse(int n,int id){
		try {
			Thread.sleep(100);
			if(n>numberToBeGuessed){
				currentPlayer.tell(new ResponseMsg(true),getSelf());
			}else if(n==numberToBeGuessed){
				currentPlayer.tell(new WinningMsg(id),getSelf());
			}else{
				currentPlayer.tell(new ResponseMsg(false),getSelf());
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private int generateRandomNumber(){
		int n=Math.abs(new Random().nextInt(200));
		System.out.println("Number to be guessed: "+n);
		return n;
	}
	
	private void createTicketDispenser(){
		ActorSystem system = ActorSystem.create("GuessTheNumber");
		this.ticketDispenser = system.actorOf(Props.create(TicketDispenser.class), "Ticket");
		this.ticketDispenser.tell(new StartGameMsg(), ActorRef.noSender());
	}
	
	
	

}
