package asg3.ex1.actors;

import java.util.Random;

import com.kenai.jffi.Main;

import akka.actor.*;
import asg3.ex1.Singleton;
import asg3.ex1.Ticket;
import asg3.ex1.ViewWrapper;
import asg3.ex1.messages.BookingMsg;
import asg3.ex1.messages.LosingMsg;
import asg3.ex1.messages.ResponseMsg;
import asg3.ex1.messages.StartTurnMsg;
import asg3.ex1.messages.TryToGuessMsg;
import asg3.ex1.messages.WinningMsg;
import src.messages.NumGuessedMsg;

public class PlayerActor extends UntypedActor {
	
	private int playerNumber;
	private Ticket ticket;
	private int numberToGuess;
	private String myName;
	private ActorRef ticketDispenser;
	
	public PlayerActor(int i,String name) {
		this.playerNumber = i;
		this.myName=name;
	}
	@Override
	public void preStart(){
		this.ticket=new Ticket(0);
		this.numberToGuess=generateNumber();
	}
	
	@Override
	public void onReceive(Object message) throws Throwable {
		// TODO Auto-generated method stub
		if(message instanceof StartTurnMsg){
			log("Player "+this.playerNumber+" created.");
			this.ticketDispenser = getContext().sender();
			this.ticketDispenser.tell(new BookingMsg(this.playerNumber),getSelf());
		}else if(message instanceof Ticket){
			int turn = ((Ticket) message).getTurn();
			this.ticket=new Ticket(turn);
			ActorRef oracle =Singleton.getInstance();
			oracle.tell(new TryToGuessMsg(numberToGuess,playerNumber,ticket.getTurn()), getSelf());
		}else if(message instanceof ResponseMsg){
			boolean isGreater = ((ResponseMsg) message).isGreater();
			if(isGreater){
				this.numberToGuess--;
				this.ticketDispenser.tell(new BookingMsg(this.playerNumber),getSelf());
			}else{
				this.numberToGuess++;
				this.ticketDispenser.tell(new BookingMsg(this.playerNumber),getSelf());
			}
		}else if(message instanceof WinningMsg){
			int id=((WinningMsg) message).getPlayerId();
			ViewWrapper.getInstance().setWinnerIDtoLabel(id);
			log("Player "+id+": WON!");
			this.ticketDispenser.tell(new WinningMsg(this.playerNumber),getSelf());
		}else if(message instanceof LosingMsg){
			int id=((LosingMsg) message).getPlayerId();
			if(this.playerNumber!=id){
				log("Player "+playerNumber+": SOB!");	
		}else if(message instanceof PoisonPill){
			log("Sono avvelenato!");
			getContext().stop(getSelf());
			
		}
			stopActor();
		}
	}
	
	private void stopActor(){
		getContext().stop(getSelf());
		log("Player "+playerNumber+": stopped.");	
	}
	
	private int generateNumber(){
		return Math.abs(new Random().nextInt(200));
	}
	
	private void log(String msg){
		  System.out.println(msg);
	  }
	
	public void setTicket(int n){
		this.ticket.setTurn(n);
	}
	
	public Ticket getTicket(){
		return this.ticket;
	}

	public int getPlayerId(){
		return this.playerNumber;
	}
}
