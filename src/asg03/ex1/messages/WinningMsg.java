package asg3.ex1.messages;

public class WinningMsg {
	
	private int playerId;
	
	public WinningMsg(int id){
		playerId=id;
	}

	public int getPlayerId() {
		return playerId;
	}

}
