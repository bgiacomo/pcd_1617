package asg3.ex1.messages;

public class BookingMsg {

	
	private int idPlayer;

	public BookingMsg(int idPlayer) {
		this.idPlayer = idPlayer;
	}
	
	public int getIdPlayer() {
		return this.idPlayer;
	}
	
	
}
