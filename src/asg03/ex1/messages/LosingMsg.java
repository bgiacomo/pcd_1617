package asg3.ex1.messages;

public class LosingMsg {

	private int playerId;
	
	public LosingMsg(int id){
		playerId=id;
	}
	
	public int getPlayerId(){
		return this.playerId;
	}
}
