package asg3.ex1.messages;

public class ResponseMsg {

	private boolean isGreater;
	
	public ResponseMsg(boolean b){
		this.isGreater=b;
	}
	
	public boolean isGreater(){
		return isGreater;
	}
	
}
