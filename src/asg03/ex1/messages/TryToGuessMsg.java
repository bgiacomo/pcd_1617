package asg3.ex1.messages;

public class TryToGuessMsg {
	
	private int number;
	private int playerId;
	private int turn;

	public TryToGuessMsg(int number,int id,int turn) {
		super();
		this.number = number;
		this.playerId = id;
		this.turn=turn;
	}
	
	public int getNumber(){
		return number;
	}
	
	public int getId(){
		return playerId;
	}
	
	public int getTurn(){
		return this.turn;
	}

}
