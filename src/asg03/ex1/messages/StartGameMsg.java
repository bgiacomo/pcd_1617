package asg3.ex1.messages;

import asg3.ex1.Util;

public class StartGameMsg {
	
	private int numPlayers;
	
	public StartGameMsg() {
		this.numPlayers = new Util().getNPlayers();
	}

	public int getNumPlayers() {
		return this.numPlayers;
	}

}

