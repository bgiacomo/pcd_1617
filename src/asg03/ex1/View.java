package asg3.ex1;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.actor.Props;
import asg3.ex1.actors.OracleActor;
import asg3.ex1.messages.KillingMsg;
import asg3.ex1.messages.StartGameMsg;

public class View extends JFrame{
	
	private static final int HEIGHT = 60;
	private static final int WIDTH = 500;
	
	private JButton startButton;
	private JButton stopButton;
	private JLabel numberLabel;
	private JLabel turnLabel;
	private JLabel winnerLabel;
	
	public View(){
		
		setSize(WIDTH, HEIGHT);
		setResizable(false);
		
		this.startButton = new JButton("START");
		this.stopButton = new JButton("STOP");
		
		JPanel panel = new JPanel();
		panel.add(startButton);		
		panel.add(stopButton);	
		
		setLayout(new BorderLayout());
	    add(panel,BorderLayout.NORTH);
	    
	    this.numberLabel=new JLabel("Guess: "+"--");
	    this.turnLabel=new JLabel("  Turn: "+0);
	    this.winnerLabel=new JLabel();
	    
	    panel.add(numberLabel);
	    panel.add(turnLabel);
	    panel.add(winnerLabel);
	    
	    this.startButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//myController = new Controller();
				createOracle();
			}
			
		});
	    
	    this.stopButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				log("Killing all the actors..");
				Singleton.getInstance().tell(new KillingMsg(), ActorRef.noSender());
			}
		});
		this.setVisible(true);
	}
	
	
	private void createOracle(){
		Singleton.getInstance();
	}
	
	private void log(String s){
		System.out.println(s);
	}
	
	public void setNumberToGuessLabel(int n){
		this.numberLabel.setText("Guess: "+n);
	}
	
	public void setRoundToTurnLabel(int n){
		this.turnLabel.setText("Turn: "+n);
	}
	
	public void setWinnerIDtoLabel(int n){
		this.winnerLabel.setText("  WinnerID: "+n);
	}
	

	
}


