package asg3.ex1;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import asg3.ex1.actors.OracleActor;
import asg3.ex1.messages.KillingMsg;
import asg3.ex1.messages.StartGameMsg;

public class Singleton {
	   
	private static ActorRef oracle = null;
	   
	   private Singleton() {
	      // Exists only to defeat instantiation.
		   this.createOracle();
	   }
	   
	   public static ActorRef getInstance() {
	      if(oracle == null) {
	    	  new Singleton();
	    	  return oracle;
	      }
	      return oracle;
	   }
	   
	   private ActorRef createOracle(){
		   ActorSystem system = ActorSystem.create("GuessTheNumber");
			this.oracle = system.actorOf(Props.create(OracleActor.class));
			oracle.tell(new StartGameMsg(), ActorRef.noSender());
			return oracle;
	   }
	   
	}