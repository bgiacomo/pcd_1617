package asg3.ex2A;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GameView extends JFrame{
	
	private static final int HEIGHT = 60;
	private static final int WIDTH = 500;
	
	private JButton startButton;
	private JLabel numberLabel;
	private JLabel turnLabel;
	private JLabel winnerLabel;
	private Controller myController;
	
	public GameView(){
		
		setSize(WIDTH, HEIGHT);
		setResizable(false);
		
		this.startButton = new JButton("START");
		
		JPanel panel = new JPanel();
		panel.add(startButton);		
		
		setLayout(new BorderLayout());
	    add(panel,BorderLayout.NORTH);
	    
	    this.numberLabel=new JLabel("Guess: "+"--");
	    this.turnLabel=new JLabel("  Turn: "+0);
	    this.winnerLabel=new JLabel();
	    
	    panel.add(numberLabel);
	    panel.add(turnLabel);
	    panel.add(winnerLabel);
	    
	    this.startButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				myController=new Controller();
			}
			
		});
	    
	
		this.setVisible(true);
	}
	
	
	public void setNumberToGuessLabel(int n){
		this.numberLabel.setText("Guess: "+n);
	}
	
	public void setRoundToTurnLabel(int n){
		this.turnLabel.setText("Turn: "+n);
	}
	
	public void setWinnerIDtoLabel(int n){
		this.winnerLabel.setText("  WinnerID: "+n);
	}
	

	
}


