package asg3.ex2A;

public class ViewWrapper {
	   
	private static GameView myView = null;
	   
	   private ViewWrapper() {
	      // Exists only to defeat instantiation.
		   myView=new GameView();
	   }
	   
	   public static GameView getInstance() {
	      if(myView == null) {
	    	  new ViewWrapper();
	    	  return myView;
	      }
	      return myView;
	   }
	   
	   
	}
