package asg3.ex2A.verticles;

import java.util.Random;

import asg3.ex2A.Util;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;

public class Player extends AbstractVerticle{

	private int id;
	private int attemptToGuess;
	
	public Player(int pid){
		id=pid;
		System.out.println("Player"+id+" ready.");
	}
	
	@Override
    public void start() throws Exception {
		super.start();
		attemptToGuess=generateRandomNumber();
		vertx.eventBus().consumer(new Util().PLAYER_CHANNEL, this::manageTicket);
		vertx.eventBus().consumer(new Util().LOSING_CHANNEL, this::printSob);
		
		//Player is asking for the ticket to the TicketDispenser
		vertx.eventBus().publish(new Util().TICKET_DISPENSER_CHANNEL, this.id);
	}
	
	private void manageTicket(Message<Object> message) {
		int playerId = (int)message.body();
		// I have to pick up the right msg from the channel
        if(playerId != this.id){
            return;
        }
        System.out.println("Player "+playerId+" got the ticket!");
        DeliveryOptions options = new DeliveryOptions();
        options.addHeader("pid", "" + playerId);
        vertx.eventBus().send(new Util().ORACLE_CHANNEL, attemptToGuess, options, msg -> {
            String body = (String) msg.result().body();
            if(body == "greater") {
                this.attemptToGuess--;
            } else if(body == "less") {
                this.attemptToGuess++;
            }
           vertx.eventBus().send(new Util().TICKET_DISPENSER_CHANNEL, this.id);
        });
	}
	
	private void printSob(Message<Object> message) {
		int pid = (int)message.body();
				
		if(this.id!=pid){
			System.out.println("Player "+this.id+" SOB!");	
		}else{
			System.out.println("Player "+this.id+" WON!");
		}
		
	}
	
	private int generateRandomNumber(){
		int n=Math.abs(new Random().nextInt(200));
		return n;
	}
	
}
