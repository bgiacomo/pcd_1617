package asg3.ex2A.verticles;

import java.util.LinkedList;
import java.util.Queue;
import asg3.ex2A.Util;
import asg3.ex2A.ViewWrapper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;

public class TicketDispenser extends AbstractVerticle{
		
	private Queue<Integer> myQueue = new LinkedList<>();
	private int deliveredTickets= 0;
	private int turn = 0;
	
	
	@Override
    public void start() throws Exception {
		System.out.println("TD is ready.");
		super.start();
		vertx.eventBus().consumer(new Util().TICKET_DISPENSER_CHANNEL, this::issue);
		vertx.eventBus().consumer(new Util().VICTORY_CHANNEL, this::endTheGame);
	}
	
	private void issue(Message<Object> message) {
		int pid = (int)message.body();
        myQueue.add(pid);
        this.deliveredTickets++;
        //Updating the current round
        if(deliveredTickets==3){
        	turn++;
        	deliveredTickets=0;
        	ViewWrapper.getInstance().setRoundToTurnLabel(turn);
        }
        if(myQueue.size() == 3) {
        	int next = myQueue.remove();
        	vertx.eventBus().publish(new Util().PLAYER_CHANNEL,next);
        }
		
	}
	
	private void endTheGame(Message<Object> message){
		int pid = (int)message.body();
		vertx.eventBus().publish(new Util().LOSING_CHANNEL, pid);
		
	}
}
