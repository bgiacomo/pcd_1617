package asg3.ex2A.verticles;

import java.util.Random;

import asg3.ex2A.Response;
import asg3.ex2A.Util;
import asg3.ex2A.ViewWrapper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;

public class Oracle extends AbstractVerticle {

	private int secretNumber;
	
	@Override
    public void start() throws Exception {
		super.start();
		this.secretNumber=generateRandomNumber();
		ViewWrapper.getInstance().setNumberToGuessLabel(secretNumber);
		vertx.eventBus().consumer(new Util().ORACLE_CHANNEL, this::handleResponse);
	}
	
	private void handleResponse(Message<Object> message) {
		  	int pid = Integer.valueOf(message.headers().get("pid"));
	        int guess = (Integer) message.body();
	        System.out.println("Player " + pid + ": is it " + guess + "?");
	        if(guess == this.secretNumber){
	            vertx.eventBus().publish(new Util().VICTORY_CHANNEL, pid);
	            try {
	                this.stop();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        } else {
	            String result = new Response(secretNumber,guess).toString();
	            message.reply(result);
	        }
	}
	
	private int generateRandomNumber(){
		int n=Math.abs(new Random().nextInt(200));
		System.out.println("Oracle: Number to be guessed is: "+n);
		return n;
	}
	
	public int getSecretNumber(){
		return secretNumber;
	}
}
