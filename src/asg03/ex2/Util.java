package asg3.ex2A;

public class Util {

	public final String ORACLE_CHANNEL = "OracleChannel";
	public final String PLAYER_CHANNEL = "PlayerChannel";
	public final String TICKET_DISPENSER_CHANNEL = "TicketChannel";
	public final String VICTORY_CHANNEL = "VictoryChannel";
	public final String LOSING_CHANNEL = "LosingChannel";
	public final int NPLAYERS = 3;

	public Util(){
		
	}
}
