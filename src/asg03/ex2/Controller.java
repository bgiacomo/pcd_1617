package asg3.ex2A;

import asg3.ex2A.verticles.Oracle;
import asg3.ex2A.verticles.Player;
import asg3.ex2A.verticles.TicketDispenser;
import io.vertx.core.Vertx;

public class Controller {

	private Oracle orcl;
	private Vertx v;
	
	public Controller(){
		orcl=new Oracle();
		
		v = Vertx.vertx();
		
		//creating the verticles
		TicketDispenser d = new TicketDispenser();
		Oracle o = new Oracle();
		v.deployVerticle(o);
		v.deployVerticle(d);
				
		for(int i = 0; i < new Util().NPLAYERS; i++) {
		     v.deployVerticle(new Player(i));
		}
	}
	
	public Oracle getOracle(){
		return orcl;
	}
	
}
