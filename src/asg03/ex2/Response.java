package asg3.ex2A;

public class Response{

    private int guess;
    private int secret;

    public Response(int secret, int guess){
        this.secret = secret;
        this.guess = guess;
    }

    public boolean isGreater() {
        return this.guess > this.secret;
    }

    public boolean isLess() {
        return this.guess < this.secret;
    }

    public String toString() {
       if(isGreater()) {
            return "greater";
        } else if(isLess()) {
            return "less";
        }
        return "";
    }
}
