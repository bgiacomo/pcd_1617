package asg3.ex3;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class View extends JFrame{
	
	private static final int HEIGHT = 60;
	private static final int WIDTH = 600;
	
	private JButton startButton;
	private JLabel a1label;
	private JLabel b1label;
	private JLabel b2label;
	
	private Controller myController;
		
	public View(){
		
		setSize(WIDTH, HEIGHT);
		setResizable(false);
		
		this.startButton = new JButton("Start sensing");
		
		JPanel panel = new JPanel();
		panel.add(startButton);		
		
		setLayout(new BorderLayout());
	    add(panel,BorderLayout.NORTH);
	    
	    this.a1label=new JLabel("A1: "+"--");
	    this.b1label=new JLabel("  B1: "+"--");
	    this.b2label=new JLabel("  B2: "+"--");
	    
	    panel.add(a1label);
	    panel.add(b1label);
	    panel.add(b2label);
	    final View v = this;
	    
	    this.startButton.addActionListener(new ActionListener(){
	    	
	    	@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("starting..");
				myController = new Controller(v);
				myController.startB1();
				myController.startB2();
				myController.checkMaxTemperature();
				try {
					myController.startA1();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
	    
		this.setVisible(true);
	}

	public JLabel getA1label() {
		return a1label;
	}

	public JLabel getB1label() {
		return b1label;
	}

	public JLabel getB2label() {
		return b2label;
	}
	
	
}
