package asg3.ex3;

import java.util.Random;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import pcd.ass03.ObservableTemperatureSensor;


public class TemperatureSensorA1 extends pcd.ass03.acme.TemperatureSensorA1 implements ObservableTemperatureSensor {

	private static final int THREAD_SLEEP_TIME = 250;
	
	private double value;
	private Random random;
	private boolean isGoing;

	public TemperatureSensorA1() {
		random = new Random();
		value = 0;
	}
	
	public Flowable<Double> createObservable() {
		return Flowable.create(new FlowableOnSubscribe<Double>() {
			@Override
			public void subscribe(FlowableEmitter<Double> subscriber) throws Exception {
				// TODO Auto-generated method stub
				new Thread(() -> {
					while (!isGoing) {
						subscriber.onNext(getCurrentValue());
						try {
							Thread.sleep(THREAD_SLEEP_TIME);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}).start();
			}
		}, BackpressureStrategy.BUFFER);	}


}