package asg3.ex3;

import java.awt.Color;
import io.reactivex.Flowable;
import pcd.ass03.acme.TemperatureSensorB1;
import pcd.ass03.acme.TemperatureSensorB2;

public class Controller {

	private static final int SUBSTRING_LENGHT = 8;
	private static final Double LIMIT = 5000.000000000000;
	private static final Double DANGER = 20.000000000000;

	private Flowable<Double> sensorA1;
	private TemperatureSensorB1 sensorB1 = new TemperatureSensorB1();
	private TemperatureSensorB2 sensorB2 = new TemperatureSensorB2();
	private Flowable<Double> sensorB1_flow;
	private Flowable<Double> sensorB2_flow;
	private View myView;

	private boolean isDangerous;
	
	public Controller(View view) {
		this.myView=view;
	}

	public void startB1(){
		this.sensorB1_flow = sensorB1.createObservable();
		sensorB1_flow.filter(x->x < LIMIT).subscribe((x) -> {
			System.out.println("b1: "+x);
			myView.getB1label().setText("  B1: "+generateSubstring(x));
		});

	}

	public void startB2(){
		this.sensorB2_flow=sensorB2.createObservable();
		sensorB2_flow.filter(value -> value<LIMIT).subscribe((value) -> {
			System.out.println("b2: "+value);
			myView.getB2label().setText("  B2: "+generateSubstring(value));
		});
	}

	public void startA1() throws InterruptedException{

		sensorA1 = new TemperatureSensorA1().createObservable();
		sensorA1.filter(value -> value<LIMIT).subscribe((value) -> {
			System.out.println("A1: "+value);
			myView.getA1label().setText("A1: "+generateSubstring(value));
		});
	}

	private String generateSubstring(double d){
		return String.valueOf(d).substring(0,SUBSTRING_LENGHT);
	}

	public void checkMaxTemperature(){
		Flowable<Double> obs3 = Flowable.combineLatest(sensorB1_flow.filter(value -> value<LIMIT), sensorB2_flow.filter(value -> value<LIMIT), (x,y) -> this.checkDanger(x, y));		
		obs3.filter(value -> value < LIMIT).subscribe((value) -> {
			if(this.isDangerous){
				myView.getB1label().setForeground(Color.red);
				myView.getB2label().setForeground(Color.red);
			}else{
				myView.getB1label().setForeground(Color.black);
				myView.getB2label().setForeground(Color.black);
			}
		});
	}
	
	private double checkDanger(double v1, double v2){
		if(v1>DANGER && v2>DANGER)
			this.isDangerous = true;
		else
			this.isDangerous=false;
		return 0;
	}


}
