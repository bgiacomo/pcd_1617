package ex2;

public interface IThreads {
	
	public void shutdown();

	public boolean getShutdownValue();
}
