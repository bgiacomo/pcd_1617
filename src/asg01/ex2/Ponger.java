package ex2;

import java.util.concurrent.*;


public class Ponger extends Thread implements IThreads {

	private static String PONGER_EXCEPTION = "Ponger exception!";
	private static int LIMIT = 100000;
	
	private MyModel model;
	private Semaphore sem;
	private Semaphore sem2;
	private Semaphore sem3;
	private boolean shutdown;
	
	public Ponger(MyModel model, Semaphore sem,Semaphore sem2, Semaphore sem3){
		this.model = model;
		this.sem=sem;
		this.sem2=sem2;
		this.sem3=sem3;
		this.shutdown=false;
	}
	
	public void run() {

		int i = 0;
		while (i < LIMIT) {
				try {
					sem2.acquire();
					if(shutdown){
						
						System.out.println("PONGER: Goodbye!");
						Singleton.getInstance().switchUnlocker();
						sem3.release();
						break;
					}
					System.out.println("Pong!");
					model.update();
					i++;

				} catch (Exception ex) {
					System.out.println(PONGER_EXCEPTION);
				}
				Singleton.getInstance().switchUnlocker();
				sem3.release();

		}
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		shutdown=true;
	}
	
	public boolean getShutdownValue(){
		return shutdown;
	}
	

}
