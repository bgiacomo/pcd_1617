package ex2;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class MyController {
	
	private MyModel model;
	Pinger pinger;
	Ponger ponger;
	Viewer viewer;
	ArrayList<IThreads> threadList=new ArrayList<>();
	
	public MyController(MyModel model){
		this.model = model;
	}
	
	public void init(){
		
		Semaphore ev=new Semaphore(1);
		Semaphore ev2=new Semaphore(0);
		Semaphore ev3=new Semaphore(0);
		initPinger(ev,ev2,ev3);
		initPonger(ev,ev2,ev3);
		initViewer(ev,ev2,ev3);
	}
	
	private void initPinger(Semaphore ev, Semaphore ev2, Semaphore ev3){
		this.pinger=new Pinger(model,ev,ev2,ev3);
		this.threadList.add(pinger);
		this.pinger.start();
		
	}
	
	private void initPonger(Semaphore ev, Semaphore ev2, Semaphore ev3){
		this.ponger=new Ponger(model,ev,ev2,ev3);
		this.threadList.add(ponger);
		this.ponger.start();
		
	}
	
	private void initViewer(Semaphore ev, Semaphore ev2, Semaphore ev3){
		this.viewer=new Viewer(model,ev,ev2,ev3);
		this.threadList.add(viewer);
		this.viewer.start();
	}
	
	public void shutdownAllThreads(){
	
		for(IThreads t: threadList){
			t.shutdown();
		}
		System.exit(-1);
		
	}
	
	

}
