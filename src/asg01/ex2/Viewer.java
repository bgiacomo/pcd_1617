package ex2;

import java.util.concurrent.Semaphore;

public class Viewer extends Thread implements IThreads {
	
	private static String VIEWER_EXCEPTION = "Viewer exception!";
	private static int LIMIT = 100000;
	
	private Semaphore sem;
	private Semaphore sem2;
	private Semaphore sem3;
	private MyModel model;
	private boolean shutdown;

	public Viewer(MyModel model) {
		super();
		this.model = model;
	}
	
	public Viewer(MyModel model,Semaphore s,Semaphore s2, Semaphore s3) {
		super();
		this.model = model;
		this.sem=s;
		this.sem2=s2;
		this.sem3=s3;
		this.shutdown=false;
	}
	
	public MyModel getModel(){
		return this.model;
	}
	
	public void run() {
		int i = 0;
		while (i < LIMIT) {
				try {
					sem3.acquire();
					if(shutdown){
						System.out.println("VIEWER: Goodbye!");
						//this.CheckSemaphore();
						/* Li sblocco entrambi, ma uno dei due sarà già terminato */
						sem2.release();
						sem.release();
						break;
					}
					System.out.println(model.getCounter());
					i++;
					//sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					System.out.println(VIEWER_EXCEPTION);
				}
				this.CheckSemaphore();
		}
	}

	private void CheckSemaphore(){
		if (Singleton.getInstance().getUnlockPonger()) {
			sem2.release();
		} else {
			sem.release();
		}
	}
	
	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		shutdown=true;
	}
	
	public boolean getShutdownValue(){
		return shutdown;
	}
	


}
