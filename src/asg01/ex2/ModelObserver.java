package ex2;

public interface ModelObserver {

	void modelUpdated(MyModel model);
}
