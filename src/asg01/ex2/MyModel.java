package ex2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class MyModel {

	private List<ModelObserver> observers;
	private int counter;
	
	public MyModel(){
		counter = 0;
		observers = new ArrayList<ModelObserver>();
	}
	
	public synchronized void update(){
		counter++;
		notifyObservers();
	}
	
	public synchronized int getState(){
		return counter;
	}
	
	public void addObserver(ModelObserver obs){
		observers.add(obs);
	}
	
	private void notifyObservers(){
		for (ModelObserver obs: observers){
			obs.modelUpdated(this);
		}
	}
	
	public int getCounter(){
		return this.counter;
	}
}
