package ex2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

class MyView extends JFrame implements ActionListener, ModelObserver {

	private MyController controller;
	private MyModel model;
	private JButton button1;
	private JButton button2;
	
	public MyView(MyController controller) {
		super("My View");
		this.controller = controller;
		
		setSize(400, 60);
		setResizable(false);
		
		this.button1 = new JButton("START");
		button1.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				model=new MyModel();			
				controller.init();
				checkButtonStop();
							
			}
			
		});

		this.button2 = new JButton("STOP");
		button2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
					controller.shutdownAllThreads();		
			}
			
		});
		
		checkButtonStop();
				
		JPanel panel = new JPanel();
		panel.add(button1);		
		panel.add(button2);	
		
		setLayout(new BorderLayout());
	    add(panel,BorderLayout.NORTH);
	    		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}
		});
	}
	
	private void checkButtonStop(){
		if(model==null){
			button2.setEnabled(false);
		}else{
			button2.setEnabled(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modelUpdated(MyModel model) {
		// TODO Auto-generated method stub
		
	}


}
