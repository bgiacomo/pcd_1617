package ex2;

public class Singleton {
	   private static Singleton instance = null;
	   private static boolean unlockPonger;
	   
	   protected Singleton() {
	      // Exists only to defeat instantiation.
	   }
	   public static Singleton getInstance() {
	      if(instance == null) {
	         instance = new Singleton();
	         unlockPonger=false;
	      }
	      return instance;
	   }
	   
	   public boolean getUnlockPonger(){
		   return unlockPonger;
	   }
	   
	   public void switchUnlocker(){
		   unlockPonger=!unlockPonger;
	   }
	}
