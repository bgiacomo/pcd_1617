package ex2;

import java.util.concurrent.*;

public class Pinger extends Thread implements IThreads {

	private static String PINGER_EXCEPTION = "Pinger exception!";
	private static int LIMIT = 100000;
	
	private MyModel model;
	private Semaphore sem;
	private Semaphore sem2;
	private Semaphore sem3;
	public boolean shutdown;

	public Pinger(MyModel model, Semaphore sem, Semaphore sem2,Semaphore sem3) {
		this.model = model;
		this.sem = sem;
		this.sem2 = sem2;
		this.sem3=sem3;
		this.shutdown=false;
	}

	public void run() {

		int i = 0;
		while (i < LIMIT) {
				try {
					sem.acquire();
					if(shutdown){
						System.out.println("PINGER: Goodbye!");
						Singleton.getInstance().switchUnlocker();
						sem3.release();
						sem2.release();
						break;
					}
					System.out.println("Ping!");
					model.update();
					i++;
					//sleep(10);

				} catch (Exception ex) {
					System.out.println(PINGER_EXCEPTION);
				}
				Singleton.getInstance().switchUnlocker();
				sem3.release();
		}

	}
	
	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		shutdown=true;
	}
	
	public boolean getShutdownValue(){
		return shutdown;
	}
	

}
