package ex1;


/**
 * Class for computing an image of a region
 * of the Mandelbrot set.
 * 
 * @author aricci
 *
 */
public class MandelbrotSetImageConcurImpl implements MandelbrotSetImage {
	
	private int w,h;
	private int image[]; 
	private Complex center;
	private double delta;
	private int nIterMax;
	private Worker[] workers;
	private int nProcessors;
		
	/**
	 * Creating an empty Mandelbrot set image. 
	 * 
	 * @param w width in pixels
	 * @param h height in pixels
	 * @param c center of the Mandelbrot set to be represented
	 * @param radius radius of the Mandelbrot set to be represented
	 */
	public MandelbrotSetImageConcurImpl(int w, int h, Complex c, double radius){
		this.w = w;
		this.h = h;
        image = new int[w*h];
        center = c;
        delta = radius/(w*0.5);
        
	}
	
	/**
	 * 
	 * Compute the image with the specified level of detail
	 * 
	 * See https://en.wikipedia.org/wiki/Mandelbrot_set
	 * 
	 * @param nIterMax number of iteration representing the level of detail
	 */
	
	public void computeConcurrently(int nIterMax) {
		// TODO Auto-generated method stub
			
			
		try {
			
			int nProcessors=Runtime.getRuntime().availableProcessors();
			Worker[] workers=new Worker[nProcessors+1];
			int step=this.getWidth()/nProcessors;
			int previousStep=0;
			int nextStep=step;
			for(int i=0;i<=nProcessors-1;i++){
				workers[i]=new Worker(previousStep,nextStep,this,nIterMax);
				workers[i].start();
				previousStep=nextStep;
				nextStep+=step;
				
			}
			int rest=this.getWidth()-previousStep;
			nextStep=rest+previousStep;
			workers[nProcessors]=new Worker(previousStep,nextStep,this,nIterMax);
			Worker lastWorker=workers[nProcessors];
			lastWorker.start();
	
			
			for(Worker w : workers){
				w.join();
			}
			
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
}
	
	public void computeConcurrentlyWithNumberOfThreads(int nIterMax, int numberOfThreads){
		
		try {
			
			Worker[] workers=new Worker[numberOfThreads];
			int step=this.getWidth()/numberOfThreads;
			int previousStep=0;
			int nextStep=step;
			for(int i=0;i<numberOfThreads-1;i++){
				workers[i]=new Worker(previousStep,nextStep,this,nIterMax);
				workers[i].start();
				previousStep=nextStep;
				nextStep+=step;
				
			}
			int rest=this.getWidth()-previousStep;
			nextStep=rest+previousStep;
			workers[numberOfThreads-1]=new Worker(previousStep,nextStep,this,nIterMax);
			Worker lastWorker=workers[numberOfThreads-1];
			lastWorker.start();
	
			
			for(Worker w : workers){
				w.join();
			}
			
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
		
	/**
	 * Basic Mandelbrot set algorithm
	 *  
	 * @param c
	 * @param maxIteration
	 * @return
	 */


	
	/**
	 * This method returns the point in the complex plane
	 * corresponding to the specified element/pixel in the image
	 * 
	 * @param x x coordinate in the image
	 * @param y y coordinate in the image
	 * @return the corresponding complex point
	 */
	public Complex getPoint(int x, int y){
		return new Complex((x - w*0.5)*delta + center.re(), center.im() - (y - h*0.5)*delta); 
	}
	
	/**
	 * Get the height of the image
	 * @return
	 */
	public int getHeight(){
		return h;
	}

	/**
	 * Get the width of the image
	 * @return
	 */
	public int getWidth(){
		return w;
	}
	
	/**
	 * Get the image as an array of int, organized per rows
	 * (compatible with the BufferedImage style)
	 * 
	 * @return
	 */
	public int[] getImage(){
		return image;
	}
	
	public void update(int pixelPosition, int value){
		image[pixelPosition]=value;
	}
	
	private double computeColor(Complex c, int maxIteration){
		int iteration = 0;		
		Complex z = new Complex(0,0);

		/*
		 * Repeatedly compute z := z^2 + c
		 * until either the point is out 
		 * of the 2-radius circle or the number
		 * of iteration achieved the max value
		 * 
		 */
		while ( z.absFast() <= 2 &&  iteration < maxIteration ){
			//System.out.println("absFast :"+z.absFast());
			z = z.times(z).plus(c); 
			iteration++;
		  }		 
		  if ( iteration == maxIteration ){	
			  //System.out.println("Iteration"+iteration);
			  /* the point belongs to the set */
			  return 0;
		  } else {
			  //System.out.println("Iteration"+iteration);

			  /* the point does not belong to the set => distance */
			  return 1.0-((double)iteration)/maxIteration;
		  }
	}

	
	
	

}

