package ex1;

public class Worker extends Thread {
	private int previousStep, nextStep, nIterations;
	private MandelbrotSetImageConcurImpl mandelbrotSet;
	private int[] subImage;

	public Worker(int previousStep, int step, MandelbrotSetImageConcurImpl mandelbrotSet, int nIterations) {
		super();
		this.previousStep = previousStep;
		this.nextStep = step;
		this.nIterations = nIterations;
		this.mandelbrotSet = mandelbrotSet;
		subImage = new int[mandelbrotSet.getHeight() * mandelbrotSet.getWidth()];

	}

	public void run() {
		System.out.println(this.getName() + "has started to compute pixels " + previousStep + " to " + nextStep);
		compute(nIterations);
		System.out.println(this.getName() + " has done");

	}

	private void compute(int nIterations) {
		for (int x = previousStep; x < nextStep; x++) {
			for (int y = 0; y < mandelbrotSet.getHeight(); y++) {
				Complex c = mandelbrotSet.getPoint(x, y);
				double level = computeColor(c, nIterations);
				int color = (int) (level * 255);
				int pxPos =  y * mandelbrotSet.getWidth() + x;
				int value = color + (color << 8) + (color << 16);
				mandelbrotSet.update(pxPos, value);

			}
		}

	}

	private double computeColor(Complex c, int maxIteration) {
		int iteration = 0;
		Complex z = new Complex(0, 0);

		/*
		 * Repeatedly compute z := z^2 + c until either the point is out of the
		 * 2-radius circle or the number of iteration achieved the max value
		 * 
		 */
		while (z.absFast() <= 2 && iteration < maxIteration) { // calcola il
																// modulo del
																// numero
																// complesso
			z = z.times(z).plus(c);
			iteration++;
		}
		if (iteration == maxIteration) {
			/* the point belongs to the set */
			return 0;
		} else {
			/* the point does not belong to the set => distance */
			return 1.0 - ((double) iteration) / maxIteration;
		}
	}

	public int getFromX() {
		return previousStep;
	}

	public int getToX() {
		return nextStep;
	}

	public int[] getSubImage() {
		return subImage;
	}

}