package pcd_ass02_ex1;

import java.util.Random;

public class Main {
	
	public static int NUMBER_OF_PLAYERS = 5;
	private static long SECRET_NUMBER=Math.abs(new Random().nextInt(200));
	
	public static void main(String[] args) {

		Oracle orcl = new Oracle(SECRET_NUMBER);
		System.out.println("SECRET NUMBER: "+SECRET_NUMBER);

		for (int i=0;i<NUMBER_OF_PLAYERS;i++){
			Player p=new Player(i,orcl);
			p.start();
		
		}
		
	}
	
	static int getNPlayers(){
		return NUMBER_OF_PLAYERS;
	}

}
