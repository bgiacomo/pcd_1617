package pcd_ass02_ex1;

public class ResultImpl implements Result {

	private long value;
	private int id;
	private boolean found;
	private boolean isGreater;
	private boolean isLess;
	private long secretNumber;
	
	public ResultImpl(int id,long numberToGuess,long secretNumber) {
		super();
		this.id=id;
		this.value = numberToGuess;
		this.setAllBooleans(secretNumber);
		this.secretNumber=secretNumber;
	}
	
	private void setAllBooleans(long secretNumber){
		
		if(this.value>secretNumber){
			this.isGreater=true;
			this.isLess=false;
			this.found=false;
		}else if(this.value<secretNumber){
			this.isGreater=false;
			this.isLess=true;
			this.found=false;
		}else{
			this.isGreater=false;
			this.isLess=false;
			this.found=true;
		}
		
	}

	@Override
	public boolean found() {
		// TODO Auto-generated method stub
		return this.found;
	}

	@Override
	public boolean isGreater() {
		// TODO Auto-generated method stub
		return this.isGreater;
	}

	@Override
	public boolean isLess() {
		// TODO Auto-generated method stub
		return this.isLess;
	}

	public long getNumberToGuess() {
		return value;
	}

	public int getID(){
		return this.id;
	}

}
