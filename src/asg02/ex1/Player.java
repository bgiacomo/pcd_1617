package pcd_ass02_ex1;

import java.util.Random;

public class Player extends Thread {
	
	private int id;
	private Oracle oracle;
	
	public Player(int id, Oracle monitor) {
		super();
		this.id = id;
		this.oracle= monitor;
	}
	@Override
	public void run(){		
		long number=this.generateRandomNumber();
		while(!oracle.isGameFinished()){
			try {
				//QUI SI BLOCCA QUANDO VIENE FATTA LA WAIT()
				Result res=oracle.tryToGuess(id, number);
				
				//Thread.sleep(100);
				if(res.isGreater()){
					number--;
				}else if(res.isLess()){
					number++;
				}

			} catch (GameFinishedException e) {
				 System.out.println("Player "+this.id+": WON!");
				 return;
			}
		
		}
		
	}
	
	private long generateRandomNumber(){
		Random rand = new Random();
		return Math.abs(rand.nextInt(200));
	}
	
	public int getID(){
		return this.id;
	}
	
}
