package pcd_ass02_ex1;

public interface OracleInterface {

	boolean isGameFinished();
	
	Result tryToGuess(int playerId, long value) throws GameFinishedException;
	
}
