package pcd_ass02_ex1;

public interface Result {
	
	boolean found();
	boolean isGreater();	
	boolean isLess();
	public int getID();
}
