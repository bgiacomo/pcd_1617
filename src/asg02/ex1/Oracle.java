package pcd_ass02_ex1;

import java.util.*;

public class Oracle implements OracleInterface {

	private long secretNumber;
	private boolean isEnded;
	private List<Result> sospendedThreads;
	
	public Oracle(long secretNumber) {
		super();
		this.secretNumber = secretNumber;
		this.isEnded=false;
		this.sospendedThreads=new LinkedList<>();
	}

	@Override
	public synchronized boolean isGameFinished() {
		return isEnded;
	}

	@Override
	public synchronized Result tryToGuess(int playerId, long value) throws GameFinishedException {
		Result res=new ResultImpl(playerId,value,secretNumber);
		this.sospendedThreads.add(res);
	
		if(sospendedThreads.size()==Main.getNPlayers()){
            notifyAll();
        }
        while((sospendedThreads.get(0).getID() != playerId || sospendedThreads.size()<Main.getNPlayers()) && !this.isEnded){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        this.checkAnswer(playerId, value, res);
        this.sospendedThreads.remove(0);
        return res;
	}
	
	 private void checkAnswer(int playerId, long value, Result res) throws GameFinishedException{
	        if(isEnded){
	        	System.out.println("Player " + playerId+ ": Sob!");
	        } else {
	            System.out.println("Player " + playerId + ": " + value);
	            if(res.found()){
	                this.isEnded = true;
	                notifyAll();
	                throw new GameFinishedException();
	            }
	        }
	    }
		


}
