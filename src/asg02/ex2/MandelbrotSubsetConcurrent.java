
public class MandelbrotSubsetConcurrent implements Runnable {

	private MandelbrotConcurrent set;
	private int start;
	private int end;
	private int nIterMax;
	private int id;

	public MandelbrotSubsetConcurrent(MandelbrotConcurrent set, int start, int end, int nIterMax, int id) {
		this.set = set;
		this.start = start;
		this.end = end;
		this.nIterMax = nIterMax;
		this.id=id;
	}

	public void run() {
		try {
			log("Task " +this.id+" started");
			set.compute(nIterMax, start, end);
			log("Task " +this.id+" done");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private void log(String msg){
		synchronized(System.out){
			System.out.println(msg);
		}
	}

}
