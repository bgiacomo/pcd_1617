

/**
 * Simple Mandelbrot Set Viewer 
 *
 * @author aricci
 *
 */
public class MandelbrotAnimator {

	/* size of the mandelbrot set in pixel */
	int width = 400;
	int height = 400;
	
	/* number of iteration */
	int nIter = 500;

	/* region to be represented: center and radius */
	/*Complex c0 = new Complex(-0.75,0);*/
	double rad0 = 2;
	
	public MandelbrotAnimator() {

		/*
		ass01.ex1.src.Complex c1 = new ass01.ex1.src.Complex(-0.75,0.1);
		double rad1 = 0.02;
		
		ass01.ex1.src.Complex c2 = new ass01.ex1.src.Complex(0.7485,0.0505);
		double rad2 = 0.000002;

		ass01.ex1.src.Complex c3 = new ass01.ex1.src.Complex(0.254,0);
		double rad3 = 0.001;
	
		*/
		
		/* creating the set */

		/*
		* Note: as the Viewer has no changes except from this call, the original MandelbrotViewer name was refactored
		* and the only change is this call (previously: ass01.ex1.src.MandelbrotSetImageImpl, now: ass01.ex1.src.MandelbrotSetImageConcurrentImpl).
		*
		* */

		Complex c4 = new Complex(0.001643721971153, 0.822467633298876);
		double radius = rad0;

		MandelbrotSetImage set = new MandelbrotConcurrent(width,height, c4, rad0);
		MandelbrotView view = new MandelbrotView(set, 410, 480);
		view.setVisible(true);
		int i = 0;
		
		while (true) {
		/* isDrawing is true: the animator can draw the image*/
			if(view.isDrawing()){
			System.out.println("Computing...");
			i=0;
			StopWatch cron = new StopWatch();
			cron.start();
		
		/* computing the image */
			set.compute(nIter);
			cron.stop();
			System.out.println("done - " + cron.getTime() + " ms");

		/* showing the image */
			set.updateRadius(radius);
			radius *= 0.9;
			view.repaint();
			}else{
				
		/* isDrawing is false: the animator is waiting for pressing the start button */
				if(i==0){
				System.out.println("Waiting...");
				i++;
				}i++;
			}
		}

	}


}
