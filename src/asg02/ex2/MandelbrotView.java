
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

/**
 * Simple view of a Mandelbrot Set Image
 * 
 * @author aricci
 *
 */
@SuppressWarnings("serial")
public class MandelbrotView extends JFrame {

	private boolean isDrawing=false;
	private MandelbrotSetImage set;
	private MandelbrotPanel canvas;
	private JScrollPane scrollPane;
	private JButton start=new JButton("START");
	private JButton stop=new JButton("STOP");


	public MandelbrotView(MandelbrotSetImage set, int w, int h) {
		super("Mandelbrot Viewer");
		setSize(w, h);
		this.setResizable(false);

		this.set = set;
		canvas = new MandelbrotPanel(set);
		canvas.setPreferredSize(new Dimension(set.getWidth(),set.getHeight()));
	    scrollPane = new JScrollPane(canvas);

	    JPanel info = new JPanel();
	   info.add(start);
	   info.add(stop);

	    setLayout(new BorderLayout());
	    add(scrollPane, BorderLayout.CENTER);
		add(info,BorderLayout.NORTH);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		
		start.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Premuto start");
				isDrawing=true;
			}
			
		});
		
		stop.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Premuto stop");
				isDrawing=false;
			}
			
		});
	}


	class MandelbrotPanel extends JPanel {

		private MandelbrotSetImage set;
		private BufferedImage image;

		public MandelbrotPanel(MandelbrotSetImage set) {
			this.set = set;
			image = new BufferedImage(set.getWidth(), set.getHeight(), BufferedImage.TYPE_INT_RGB);
		}

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			image.setRGB(0, 0, set.getWidth(), set.getHeight(), set.getImage(), 0, set.getWidth());
			g2.drawImage(image, 0, 0, null);
		}

	}
	
	public boolean isDrawing(){
		return this.isDrawing;
	}

}
